import { useEffect, useState } from "react";
import Winscreen from "./components/Winscreen";

function App() {
  const [numbers, setNumbers] = useState([1, 1, 1]);
  const [isRolling, setIsRolling] = useState(false);
  const [message, setMessage] = useState("");
  const [win, setWin] = useState(false);
  const [player1Score, setPlayer1Score] = useState(0);
  const [player2Score, setPlayer2Score] = useState(0);
  const [currentPlayer, setCurrentPlayer] = useState(1);
  const [rerollingSixes, setRerollingSixes] = useState(false);
  const [reset, setReset] = useState(false);
  const [disabled, setDisabled] = useState(false);

  useEffect(() => {
    if (reset) {
      setNumbers([1, 1, 1]);
      setIsRolling(false);
      setMessage("");
      setWin(false);
      setPlayer1Score(0);
      setPlayer2Score(0);
      setCurrentPlayer(1);
      setRerollingSixes(false);
      setReset(false);
      setDisabled(false);
    }
  }, [reset]);

  const rollDice = (rerollIndexes = []) => {
    setIsRolling(true);
    setMessage("");
    setTimeout(() => {
      let newNumbers = numbers.map((num, idx) =>
        rerollIndexes.includes(idx) ? Math.floor(Math.random() * 6) + 1 : num
      );

      if (rerollIndexes.length === 0) {
        newNumbers = [
          Math.floor(Math.random() * 6) + 1,
          Math.floor(Math.random() * 6) + 1,
          Math.floor(Math.random() * 6) + 1,
        ];
      }
      setNumbers(newNumbers);

      setIsRolling(false);

      // Trouver les index de tous les dés qui ont obtenu un 6
      const sixIndexes = newNumbers.reduce((indexes, num, idx) => {
        if (num === 6) {
          indexes.push(idx);
        }
        return indexes;
      }, []);

      if (sixIndexes.length > 0 && !rerollingSixes) {
        setDisabled(true);

        setTimeout(() => {
          const newRolls = newNumbers.map((num, idx) =>
            sixIndexes.includes(idx) ? Math.floor(Math.random() * 5) + 1 : num
          );
          setNumbers(newRolls);
          const newTotal = newRolls.reduce((acc, num) => acc + num, 0);
          setTimeout(() => {
            if (currentPlayer === 1) {
              const updatedScore = player1Score + newTotal;
              setPlayer1Score(updatedScore);
              const sortedNumbers = [...newNumbers].sort();
              if (sortedNumbers.join("") === "124") {
                setMessage(
                  `Félicitations, joueur 1, vous avez gagné avec la combinaison 4-2-1!`
                );
                setWin(true);
                setDisabled(true);
              }
              if (updatedScore >= 30) {
                setMessage(
                  "Félicitations, joueur 1, vous avez atteint 30 points et gagné!"
                );
                setWin(true);
                setDisabled(true);
              } else {
                setCurrentPlayer(2);
              }
            } else {
              const updatedScore = player2Score + newTotal;
              setPlayer2Score(updatedScore);
              const sortedNumbers = [...newNumbers].sort();
              if (sortedNumbers.join("") === "124") {
                setMessage(
                  `Félicitations, joueur 2, vous avez gagné avec la combinaison 4-2-1!`
                );
                setWin(true);
                setDisabled(true);
              }
              if (updatedScore >= 30) {
                setMessage(
                  "Félicitations, joueur 2, vous avez atteint 30 points et gagné!"
                );
                setWin(true);
                setDisabled(true);
              } else {
                setCurrentPlayer(1);
              }
            }
          }, 2000);
          setDisabled(false);

          setCurrentPlayer(currentPlayer === 1 ? 2 : 1);
        }, 1000);
      }
      setTimeout(() => {
        if (!newNumbers.includes(6)) {
          const newTotal = newNumbers.reduce((acc, num) => acc + num, 0);

          const sortedNumbers = [...newNumbers].sort();
          if (sortedNumbers.join("") === "124") {
            setMessage(
              `Félicitations, joueur ${currentPlayer}, vous avez gagné avec la combinaison 4-2-1!`
            );
            setWin(true);
            setDisabled(true);
          } else {
            if (currentPlayer === 1) {
              const updatedScore = player1Score + newTotal;
              setPlayer1Score(updatedScore);
              if (updatedScore >= 30) {
                setMessage(
                  "Félicitations, joueur 1, vous avez atteint 30 points et gagné!"
                );
                setWin(true);
                setDisabled(true);
              }
            } else {
              const updatedScore = player2Score + newTotal;
              setPlayer2Score(updatedScore);
              if (updatedScore >= 30) {
                setMessage(
                  "Félicitations, joueur 2, vous avez atteint 30 points et gagné!"
                );
                setWin(true);
                setDisabled(true);
              }
            }
          }
        }
      }, 1000);
      // Changer de joueur
      setCurrentPlayer(currentPlayer === 1 ? 2 : 1);
    }, 1000); // Durée de l'animation
  };

  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-neutral-900 text-white">
      <h1
        className={
          currentPlayer === 1
            ? "text-5xl font-bold text-blue-300"
            : "text-5xl font-bold text-yellow-300"
        }
      >
        Lancer les dés Joueur {currentPlayer}
      </h1>
      <button
        onClick={() => rollDice()}
        className="px-6 py-3 text-2x border border-white bg-neutral-700 hover:bg-neutral-500 font-bold rounded mt-8"
        disabled={isRolling || rerollingSixes || disabled}
      >
        {isRolling ? "Lancer en cours..." : `Lancer les dés`}
      </button>
      <div className="mt-4 text-2xl w-full">
        <p>
          Somme des points des dés :{" "}
          {numbers.reduce((acc, num) => acc + num, 0)}
        </p>
        <div className="flex justify-around">
          <p className="font-bold text-blue-300">
            Score du joueur 1 : {player1Score}
          </p>
          <p className="font-bold text-yellow-300">
            Score du joueur 2 : {player2Score}
          </p>
        </div>
      </div>
      {win ? <Winscreen message={message} setReset={setReset} /> : null}
      <div className="flex gap-24">
        {" "}
        {/* Ajout de l'espace entre les cubes */}
        {numbers.map((number, index) => (
          <div key={index} className="scene">
            <div
              className={`cube ${isRolling ? "rolling" : ""} face-${number}`}
            >
              <div className="cube__face cube__face--front">{number}</div>
              <div className="cube__face cube__face--back">{number}</div>
              <div className="cube__face cube__face--right">{number}</div>
              <div className="cube__face cube__face--left">{number}</div>
              <div className="cube__face cube__face--top">{number}</div>
              <div className="cube__face cube__face--bottom">{number}</div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;
