export default function Winscreen({ message, setReset }) {
  return (
    <div className="flex flex-col items-center pt-6 pb-6">
      <h1 className="text-green-500 font-bold text-4xl">{message}</h1>
      <img
        src="https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExNzlsNjdvN2JibnJ2dXdrdTNvcW4xd3ozanIwMjl1cWh0YWljaDk3dSZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/XfmpKFHyTRESlTmzTm/giphy-downsized-large.gif"
        alt="Gif de célébration"
        className="w-96"
      />
      <button
        className="px-6 py-3 text-2x border border-white bg-neutral-700 hover:bg-neutral-500 font-bold rounded mt-8"
        onClick={() => setReset(true)}
      >
        Rejouer
      </button>
    </div>
  );
}
